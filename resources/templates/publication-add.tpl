% rebase('base.tpl')

<form method="post">
        <h4>Paste Pubmed IDs (one per line)</h4>
        <textarea class="input-block-level" rows="5" name="pubmed_id"></textarea></td>
        <button class="btn btn-primary" type="submit" name="submit" value="pubmed_id">Submit</button>
    <hr />
        <h4>Import from Pubmed search</h4>
        <input class="input-block-level" type="text" size="40" name="pubmed_query" />
        <button class="btn btn-primary" type="submit" name="submit" value="pubmed_query">Submit</button>
    <hr />
        <h4>Import URL</h4>
        <input class="input-block-level" type="text" size="40" name="url" />
        <button class="btn btn-primary" type="submit" name="submit" value="url">Submit</button>
</form>
