% rebase('base.tpl')
<table>
    <thead>
        <td>Author</td>
        <td>Year</td>
        <td>Title</td>
        <td>Venue</td>
        <td>Notes</td>
        <td>PDF</td>
    </thead>
    <tbody>
    % for publication in publications:
    <tr>
        <td>{{publication.authors[0]}}
        %   if len(publication.authors) > 1:
                et al.
        %   end
        </td>
        <td>{{publication.date.year if publication.date else ""}}</td>
        <td>{{publication.title}}</td>
        <td>{{publication.venue()}}</td>
        <td>
            <a href="/note/edit/{{publication.note.id}}" />notes</a>
        </td>
        <td>
        %   if publication.files:
            <a href="/file/{{publication.files[0].id}}">pdf</a>
        %   end
        </td>
    </tr>
% end
    </tbody>
</table>
