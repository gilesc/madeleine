<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Madeleine reference manager">
    <meta name="author" content="Cory Giles">

    <!-- Basic theme -->
    <link rel="stylesheet" href="/static/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/static/css/darkstrap.min.css" />
    <script src="http://code.jquery.com/jquery-latest.js"></script>
    <script src="/static/js/bootstrap.min.js"></script>

    <!-- JQuery DataTables -->
    <link rel="stylesheet" href="//cdn.datatables.net/plug-ins/28e7751dbec/integration/bootstrap/3/dataTables.bootstrap.css" />
    <script type="text/javascript" language="javascript"
        src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/28e7751dbec/integration/bootstrap/3/dataTables.bootstrap.js"></script>

    <!-- CodeMirror -->
    <link rel="stylesheet" type="text/css" 
        href="//cdnjs.cloudflare.com/ajax/libs/codemirror/4.2.0/codemirror.css" />
    <link rel="stylesheet" type="text/css" 
        href="//cdnjs.cloudflare.com/ajax/libs/codemirror/4.2.0/theme/solarized.css" />
    <script type="text/javascript" language="javascript" 
        src="//cdnjs.cloudflare.com/ajax/libs/codemirror/4.2.0/codemirror.min.js"></script>
    <script type="text/javascript" language="javascript" 
        src="//cdnjs.cloudflare.com/ajax/libs/codemirror/4.2.0/keymap/vim.js"></script>
    <script type="text/javascript" language="javascript" 
        src="//cdnjs.cloudflare.com/ajax/libs/codemirror/4.2.0/addon/mode/overlay.js"></script>
    <script type="text/javascript" language="javascript" 
        src="//cdnjs.cloudflare.com/ajax/libs/codemirror/4.2.0/mode/rst/rst.js"></script>

    <script type="text/javascript" language="javascript">
/*
$(function() {
    $('.data-table').dataTable({
        
    });
    var textArea = document.getElementById("code");
    var editor = CodeMirror.fromTextArea(textArea, {
        vimMode: true,
        mode: "rst",
        theme: "solarized"
    });
})
*/
    </script>

</head>
<body>

<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a class="brand" href="/">Madeleine</a>
            <div class="nav-collapse collapse">
                <ul class="nav">
                    <li><a href="/">Browse</a></li>
                    <li><a href="/publication/add">Add</a></li>
                    <li><a href="/options">Options</a></li>
                    <li><a href="/statistics">Statistics</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>

<!--Anchor top-->
<a id="anchor-top"></a><br />

<div class="container" id="content">
    <br />
    <br />

    <div id="base">
        {{!base}}
    </div>
</div>

</body>
</html>
