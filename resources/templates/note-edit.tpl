% rebase('base.tpl')

<form method="post">
    <h4>Title:</h4>
    <input type="text" value="{{note.title if note.content else ""}}" name="title" size="80"> <br />
    <h4>Content:</h4>
    <textarea name="content" rows="40" cols="80">{{note.content if note.content else ""}}</textarea>
    <br />
    <input name="edit" value="Edit" type="submit">
</form>
