% rebase('base.tpl')

<h2>Database statistics</h2>

<div class="row">
    <div class="span4">
        <h4>Database contents</h4>
        {{!general_statistics}}
    </div>
    <div class="span4">
        <h4>Top authors</h4>
        {{!top_authors}}
    </div>
    <div class="span4">
        <h4>Top journals</h4>
        {{!top_journals}}
    </div>
</div>
