% rebase('base.tpl')

<div class="row">
    <div class="span12">
        <h3>{{pub.title}}</h3>
    </div>
</div>

<div class="row">
    <div class="span12">
        <button name="save" type="submit" class="btn btn-primary">Save changes</button>
        % if pub.pdf is not None:
            <button name="view_pdf" type="submit" class="btn">View PDF</button>
        % end
        <button name="delete" type="submit" class="btn btn-danger">Delete record</button>
    </div>
</div>

<div class="row">
    <div class="span12">
        <legend>Citation Details</legend>
        <form class="form-horizontal">
            <fieldset>
                <div class="control-group">
                    <label class="control-label" for="title">Title</label>
                    <div class="controls">
                        <textarea id="title" rows="2" class="input-block-level">{{pub.title}}</textarea>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="authors">Authors</label>
                    <div class="controls">
                        <textarea id="authors" rows="2" class="input-block-level">{{", ".join(pub.authors)}}</textarea>
                    </div>
                </div>
                % if hasattr(pub, "url"):
                    <div class="control-group">
                        <label class="control-label" for="url">URL</label>
                        <div class="controls">
                            <input class="input-block-level" type="text" id="url" value="{{pub.url}}"> 
                        </div>
                    </div>
                % end
            </fieldset>
        </form>
    </div>
</div>

<div class="row" id="notes">
    <div class="span4">
        <h4>Attachments</h4>
        <ul>
        % if pub.files:
            % for file in pub.files:
                <li><a href="/file/{{file.id}}">{{file.name}}</a></li>
            % end
        % else:
            <p>(No attachments)</p>
        % end 
        </ul>
        
        <hr />
        <h5>Upload an attachment</h5>
        <form class="form-horizontal">
            <input type="file" name="file">
            <button class="btn btn-primary" type="submit">Upload</button>
       </form> 
    </div>
    <div class="span8">
        <h4>Notes</h4>
        <textarea class="input-block-level" rows="15" id="note" name="note">{{pub.note.content or ""}}</textarea>
    </div>
</div>
