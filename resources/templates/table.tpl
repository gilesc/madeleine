% rebase('base.tpl')

<table class="data-table table">
    <thead>
    <tr>
    % for col in frame.columns:
        <td>{{!col}}</td>
    % end
    </tr>
    </thead>

    <tbody>
    % for row in frame.to_records(index=False):
    <tr>
        % for item in row:
        <td>{{!item}}</td>
        % end
    </tr>
    % end

    </tbody>
</table>

<script type="text/javascript" language="javascript">
$(function() {
    $('.data-table').dataTable({
        aoColumns: [
            {"sWidth": "10%"},
            {"sWidth": "5%"},
            {"sWidth": "45%"},
            {"sWidth": "30%"},
            {"sWidth": "5%"},
            {"sWidth": "5%"}
        ]
    });
})
</script>
