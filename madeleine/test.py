from madeleine.model import db, create_tables
from madeleine.ext import pubmed

def load_data():
    create_tables()

    for pubmed_id in [24267974, 24267917]:
        pubmed.add(db, pubmed_id)
