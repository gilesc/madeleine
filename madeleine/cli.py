import argparse
import sys

import bottle

from madeleine.model import create_tables

import click

@click.group()
def cli():
    pass

@cli.command()
@click.option("--port", "-p", type=int, default=9000)
@click.option("--debug", "-d", is_flag=True)
@click.option("--reload", "-r", is_flag=True)
def serve(port=9000, debug=False, reload=False):
    """
    Start the madeleine HTTP server
    """
    from madeleine.route import root
    create_tables()
    if debug:
        bottle.debug(True)
    bottle.run(root, host="0.0.0.0", port=port, reloader=reload)

if __name__ == "__main__":
    cli()
