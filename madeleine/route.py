import os
import urllib.parse

import bottle
from bottle import Bottle, static_file, view, response, request, redirect
from sqlalchemy.orm import with_polymorphic
from sqlalchemy import func
import pandas as pd

from centrum import ResourceLookup
from centrum.web import Application, link

from madeleine.model import *
from madeleine.ext import pubmed

resource = ResourceLookup("madeleine")
TEMPLATE_ROOT = resource.path("templates")
STATIC_ROOT = resource.path("static")

bottle.TEMPLATE_PATH.insert(0, TEMPLATE_ROOT)

root = Application()

#########
# Helpers
#########

def render_table(df, index=False, data_table=False):
    header = True
    if isinstance(df, pd.Series):
        df = df.to_frame()
        index = True
        header = False
    classes = "data-table table" if data_table else "table"
    return df.to_html(header=header, index=index, classes=classes)

def link(text, url):
    return '<a href="%s">%s</a>' % (url, text)

PublicationPolymorphic = with_polymorphic(Publication, 
        Publication.__subclasses__())

def get_publication_by_id(id):
    return db.query(PublicationPolymorphic).filter_by(id=id).first()

################
# General routes
################

@root.route("/static/<path:path>")
def dispatch(path):
    return static_file(path, root=STATIC_ROOT)

@root.route("/statistics")
@view("statistics.tpl")
def dispatch():
    tables = {}
    # General statistics
    index = ["Articles", "Books", "Links", "Notes", "PDFs", "Tags"]
    data = [
            db.query(Article).count(),
            db.query(Book).count(),
            db.query(Link).count(),
            db.query(Note).filter(Note.content != None).count(),
            db.query(File).filter_by(mime_type="application/pdf").count(),
            db.query(Tag).count()
    ]
    tables["general_statistics"] = pd.Series(data, index=index)
    # Top authors
    index, authors = zip(*db.query(Author.display_name, 
                func.count(Author.display_name))\
            .join(publication_author)\
            .group_by(Author.display_name)\
            .order_by(func.count(Author.display_name).desc())\
            .limit(len(data)))
    tables["top_authors"] = pd.Series(authors, index=index)
    # Top journals
    index, journals = zip(*db.query(Journal.display_name, 
                func.count(Journal.display_name))\
            .join(JournalVolume).join(JournalIssue).join(Article)\
            .group_by(Journal.display_name)\
            .order_by(func.count(Journal.display_name).desc())\
            .limit(len(data)))
    tables["top_journals"] = pd.Series(journals, index=index)

    return dict((k,render_table(v)) for (k,v) in tables.items())

#############
# Publication
#############

@root.get("/publication/add")
@view("publication-add.tpl")
def dispatch():
    return

@root.post("/publication/add")
@view("publication-add.tpl")
def dispatch():
    method = request.forms.get("submit")
    if method == "pubmed_id":
        for line in request.forms.get("pubmed_id").split("\n"):
            try:
                pubmed_id = int(line)
            except ValueError:
                continue
            pubmed.add(db, pubmed_id)
    elif method == "pubmed_query":
        term = request.forms.get("pubmed_query")
        for pubmed_id in pubmed.search(term):
            pubmed.add(db, pubmed_id)
    elif method == "url":
        url = request.forms.get("url")
        parsed_url = urllib.parse.urlparse(url)
        if parsed_url.scheme == "":
            parsed_url.scheme = "http"
        url = urllib.parse.urlunparse(parsed_url)
        db.add(Link(url))
        db.commit()

@root.route("/publication/view/<id>")
@view("publication-view.tpl")
def dispatch(id):
    return {"pub": get_publication_by_id(id)}

@root.route("/")
@root.route("/publication/list")
@view("table.tpl")
def fn():
    entity = with_polymorphic(Publication, Publication.__subclasses__())
    rows = []
    columns = ["Author", "Year", "Title", "Venue", "Details", "PDF"]
    for pub in db.query(entity).order_by(entity.date_published.desc()).all():
        if pub.authors:
            author = pub.authors[0]
            if len(pub.authors) > 1:
                author += " et al."
        else:
            author = "-"
        title = pub.title or ""
        if title and len(title) > 120:
            title = title[:119] + "..."
        file = ""
        if pub.files:
            file = link("pdf", "/file/%s" % pub.files[0].id)
        rows.append((
            author, 
            pub.date_published.year if pub.date_published else "",
            title,
            pub.venue(),
            link("view", "/publication/view/%s" % pub.id),
            file))

    if len(rows) == 0:
        rows.append(["No data"] * len(columns))
    table = pd.DataFrame(rows, columns=columns)
    return {"frame": table}

@root.get("/options")
@view("options.tpl")
def fn():
    return

@root.get("/note/edit/<id>")
@view("note-edit.tpl")
def fn(id):
    return {"note": db.query(Note).get(id)}

@root.get("/note/edit/<id>")
@view("note-edit.tpl")
def fn(id):
    return {"note": db.query(Note).get(id)}

@root.post("/note/edit/<id>")
@view("note/edit.tpl")
def fn(id):
    note = db.query(Note).get(id)
    note.title = request.forms.get("title")
    note.content = request.forms.get("content")
    db.add(note)
    db.commit()
    return {"note": note}

import docutils.core

@root.route("/note/view/<id>")
def fn(id):
    note = db.query(Note).get(id)
    rst = ""
    if note.title:
        barrier = "".join(["="] * len(note.title))
        rst = "%s\n%s\n%s\n" % (barrier, note.title, barrier)
    rst += note.content
    return docutils.core.publish_string(rst, writer_name="html")

@root.route("/file/<id>")
def fn(id):
    file = db.query(File).get(id)
    response.content_type = file.mime_type
    return file.data
