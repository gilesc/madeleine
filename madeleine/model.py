__all__ = ["db", "get_or_create",
    "publication_tag", "publication_author", 
    "Publication", "Article", "Book", "Link",
    "Author", 
    "File", "Tag", "Note",
    "Journal", "JournalVolume", "JournalIssue"]

import abc
import datetime

from sqlalchemy import create_engine, Column, Integer, String, \
    LargeBinary, Table, Date
from sqlalchemy.schema import ForeignKey
from sqlalchemy.orm import sessionmaker, deferred, relationship, backref
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.ext.hybrid import hybrid_property
import lxml.html

from centrum.web import link

Base = declarative_base()

publication_tag = Table("publication_tag", Base.metadata,
        Column("publication_id", Integer, ForeignKey("publication.id")),
        Column("tag_id", Integer, ForeignKey("tag.id")))

# FIXME: does this produce a stable ordering of the author list
# (assuming the authors are added in order)?
# -- no, but can use relationship(*args, order_by="foo")
publication_author = Table("publication_author", Base.metadata,
        Column("publication_id", Integer, ForeignKey("publication.id")),
        Column("author_id", Integer, ForeignKey("author.id")))

class Publication(Base):
    __tablename__ = "publication"
    __mapper_args__ = {
        "polymorphic_identity": "publication",
        "polymorphic_on": "type"
    }

    def __init__(self, **kwargs):
        self.date_added = datetime.date.today()

    id = Column(Integer, primary_key=True)
    key = Column(String, unique=True)
    type = Column(String)

    _authors = relationship("Author", secondary=lambda: publication_author)
    authors = association_proxy("_authors", "display_name",
            creator=lambda firstlast: \
                    Author(first=firstlast[0], last=firstlast[1]))

    title = Column(String)
    date_published = Column(Date)
    date_added = Column(Date)

    tags = relationship("Tag", secondary=publication_tag)
    files = relationship("File", backref="publication")
    note = relationship("Note", backref="publication", uselist=False)

    def __init__(self, *args, **kwargs):
        self.note = Note()
        super(Publication, self).__init__(*args, **kwargs)

    @abc.abstractmethod
    def venue(self):
        return

    @property
    def pdf(self):
        for file in self.files:
            if file.mime_type == "application/pdf":
                return file

class Author(Base):
    __tablename__ = "author"

    def __init__(self, first, last):
        self.first = first
        self.last = last
        if self.first is not None:
            first = "".join([n[0] for n in self.first.split()]).upper()
            self.display_name = "%s %s" % (self.last, first)
        else:
            self.display_name = self.last

    id = Column(Integer, primary_key=True)
    first = Column(String)
    last = Column(String)
    display_name = Column(String)

class Tag(Base):
    __tablename__ = "tag"

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)
    publications = relationship("Publication", 
            secondary=publication_tag)

# Files and Notes can either be standalone or attached to a Publication

class File(Base):
    __tablename__ = "file"

    id = Column(Integer, primary_key=True)
    publication_id = Column(Integer, ForeignKey("publication.id"))

    name = Column(String)
    mime_type = Column(String)
    data = deferred(Column(LargeBinary))

class Note(Base):
    __tablename__ = "note"

    id = Column(Integer, primary_key=True)
    publication_id = Column(Integer, ForeignKey("publication.id"))

    title = Column(String)
    content = Column(String)

########################
# Journal-related tables
########################

class Journal(Base):
    __tablename__ = "journal"

    def __init__(self, *args, **kwargs):
       super(Journal,self).__init__(*args, **kwargs)

    id = Column(Integer, primary_key=True)
    issn = Column(String)
    name = Column(String)
    display_name = Column(String)
    iso_abbreviation = Column(String)

    def __init__(self, *args, **kwargs):
        super(Journal, self).__init__(*args, **kwargs)
        if self.name is not None:
            tokens = []
            for token in self.name.split("(")[0].split(":")[0].split():
                token = token[0].upper() + token[1:]
                tokens.append(token)
            self.display_name = " ".join(tokens)

    volumes = relationship("JournalVolume", backref="journal")

class JournalVolume(Base):
    __tablename__ = "journal_volume"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    
    journal_id = Column(Integer, ForeignKey("journal.id"))
    issues = relationship("JournalIssue", backref="volume")

class JournalIssue(Base):
    __tablename__ = "journal_issue"

    id = Column(Integer, primary_key=True)
    name = Column(String)

    journal_volume_id = Column(Integer, ForeignKey("journal_volume.id"))
    articles = relationship("Article", backref="journal_issue")

###################
# Publication types
###################

class Article(Publication):
    __tablename__ = "article"
    __mapper_args__ = {
        "polymorphic_identity": "article"
    }

    id = Column(Integer, ForeignKey("publication.id"), primary_key=True)
    abstract = Column(String)

    journal_issue_id = Column(Integer, ForeignKey("journal_issue.id"))

    # xrefs (optional)
    pubmed_id = Column(Integer, unique=True, nullable=True)
    pmc_id = Column(Integer, unique=True, nullable=True)
    doi = Column(String, unique=True, nullable=True)

    def venue(self):
        return self.journal_issue.volume.journal.iso_abbreviation

class Book(Publication):
    __tablename__ = "book"
    __mapper_args__ = {
        "polymorphic_identity": "book"
    }

    id = Column(Integer, ForeignKey("publication.id"), primary_key=True)
    isbn = Column(String)

    def venue(self):
        return "publisher"

class Link(Publication):
    __tablename__ = "link"
    __mapper_args__ = {
        "polymorphic_identity": "link"
    }
    id = Column(Integer, ForeignKey("publication.id"), primary_key=True)
    url = Column(String, nullable=False, unique=True)

    def __init__(self, url, **kwargs):
        self.url = url
        try:
            self.title = lxml.html.parse(url).find(".//title").text
        except:
            pass
        super(Link,self).__init__(**kwargs)

    def venue(self):
        return link(self.url, self.url)

#########
# Helpers
#########

def get_or_create(session, model, defaults=None, **kwargs):
    from sqlalchemy.sql.expression import ClauseElement

    instance = session.query(model).filter_by(**kwargs).first()
    if instance:
        return instance, False
    else:
        params = dict((k, v) for k, v in kwargs.items() \
                if not isinstance(v, ClauseElement))
        params.update(defaults or {})
        instance = model(**params)
        session.add(instance)
        return instance, True

####################
# Session management
####################

def get_session(create=False):
    #engine = create_engine("sqlite:///madeleine.db")
    #engine = create_engine("sqlite:////dev/shm/madeleine.db")
    engine = create_engine("postgresql+psycopg2://localhost/madeleine")
    Session = sessionmaker()
    Session.configure(bind=engine)
    if create:
        Base.metadata.create_all(engine)
    return Session()

def create_tables():
    db = get_session(create=True)
    db.commit()

db = get_session()
