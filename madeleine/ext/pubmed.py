import datetime
import io
import urllib.request
import urllib.parse

from lxml import etree

from madeleine.model import *
from madeleine.ext import pmc
from centrum import open_url

EUTILS_BASE = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/"

def search(term):
    term = urllib.parse.quote(term)
    url = "%s%s?db=pubmed&retmax=200&term=%s" % (EUTILS_BASE, "esearch.fcgi?", term)
    root = etree.parse(open_url(url))
    return list(map(int, root.xpath("IdList/Id/text()")))

def get_or_create_journal_issue(db, 
        journal_issn=None, 
        journal_name=None,
        journal_iso_abbreviation=None,
        journal_volume=None, 
        journal_issue=None):

    journal,_ = get_or_create(db, Journal,
            issn=journal_issn,
            name=journal_name,
            iso_abbreviation=journal_iso_abbreviation)
    
    volume = db.query(JournalVolume)\
            .filter_by(name=journal_volume, journal_id=journal.id)\
            .first()
    if volume is None:
        volume = JournalVolume(journal_id=journal.id,
                name=journal_volume)
        db.add(volume)

    issue = db.query(JournalIssue)\
            .filter_by(name=journal_issue, journal_volume_id=volume.id)\
            .first()
    if issue is None:
        issue = JournalIssue(journal_volume_id=volume.id,
                name=journal_issue)
        db.add(issue)
    db.commit()
    return issue

def node_text(node, path, transform=None, default=None):
    try:
        data = node.xpath(path + "/text()")[0]
    except IndexError:
        return default
    if transform:
        try:
            data = transform(data)
        except:
            data = default
    return data

def add(db, pubmed_id, fetch_pdf=True):
    method = "efetch.fcgi"
    url = "%s%s?db=pubmed&id=%s&rettype=xml&retmode=xml" % \
            (EUTILS_BASE, method, pubmed_id)
    handle = open_url(url)

    tree = etree.parse(handle)
    citation = tree.getroot()[0][0]
    xref = {}
    for node in tree.getroot()[0].xpath("PubmedData/ArticleIdList/ArticleId"):
        key = node.get("IdType")
        value = node.text
        if key not in ("pubmed", "pmc", "doi"):
            continue
        if key in ("pubmed", "pmc"):
            key += "_id"
            if key == "pmc_id":
                value = value[3:]
            value = int(value)
        xref[key] = value

    assert(xref["pubmed_id"] == pubmed_id)

    if db.query(Article).filter_by(pubmed_id=pubmed_id).first() is not None:
        handle.close()
        return

    node = citation.xpath("Article")[0]

    journal_issue = get_or_create_journal_issue(db,
        journal_issn = node_text(node,"Journal[1]/ISSN"),
        journal_name = node_text(node,"Journal[1]/Title"),
        journal_iso_abbreviation = \
            node_text(node, "Journal[1]/ISOAbbreviation"),
        journal_volume = node_text(node, "Journal[1]/JournalIssue/Volume"),
        journal_issue = None)

    title = node_text(node, "ArticleTitle")
    abstract = " ".join(node.xpath("Abstract/AbstractText/text()"))
    date = datetime.date(
        node_text(citation, "DateCompleted/Year", transform=int, default=1),
        node_text(citation, "DateCompleted/Month", transform=int, default=1),
        node_text(citation, "DateCompleted/Day", transform=int, default=1))
    if date.year == 1:
        date = None

    article = Article(
            title=title,
            abstract=abstract,
            date_published=date,
            journal_issue=journal_issue,
            **xref)
            
    for i,author in enumerate(node.xpath("AuthorList/Author")):
        last = node_text(author, "LastName")
        first = node_text(author, "ForeName")
        article.authors.append((first, last))

    db.add(article)
    db.commit()
    handle.close()

    if fetch_pdf and article.pmc_id is not None:
        pdf = pmc.fetch_pdf(article.pmc_id)
        if pdf is not None:
            file = File(
                    name="PMID%s.pdf" % pubmed_id,
                    mime_type="application/pdf",
                    data=pdf.read())
            article.files.append(file)
    return article
