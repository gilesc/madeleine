from centrum import open_url

from lxml import etree

PMC_OA = "http://www.pubmedcentral.nih.gov/utils/oa/oa.fcgi?"

def fetch_pdf(pmc_id):
    url = "%sid=%s" % (PMC_OA, "PMC" + str(pmc_id))
    with open_url(url) as handle:
        root = etree.parse(handle)
        links = root.xpath("records/record/link[@format='pdf']/@href")
        if links:
            return open_url(links[0], mode="rb")
