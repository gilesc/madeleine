General
=======

- config file
- configurable base URL

User management
===============

- Allow multi-user mode?

Import
======

From Pubmed query
-----------------

- Allow user to pick which results from the query to use

Options
=======

- warn on deletion
- records per page on listing, maybe configure displayed columns
- fetch PDFs from PMC? (or maybe a checklist of sources to fetch from)
- truncate title at X characters
