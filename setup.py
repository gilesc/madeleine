from setuptools import setup

setup(
    name="madeleine",
    author="Cory Giles",
    author_email="mail@corygil.es",
    entry_points={
        "console_scripts": [
            "madeleine=madeleine.cli:cli"
        ]
    }
)
